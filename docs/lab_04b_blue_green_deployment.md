# Blue / Green Deployments with GitLab

## Overview

You may have noticed that the version of Wordpress the server is currently running with is outdated, there are newer versions available.  To deploy an updated version of Wordpress you will adopt a [blue / green deployment strategy](https://martinfowler.com/bliki/BlueGreenDeployment.html) to maintain production operations, but alongside production deploy a new system that can be tested before going live.  

There are many ways to perform a blue / green deployment and they are all dependant upon the technologies which make up your product.  The implementation being demonstrated today will show how to use GitLab, Terraform, Ansible, and AWS to deploy a staged instance of Wordpress alongside a production instance.  This is one way to perform a blue / green deployment but is not the only way.  Please experiment with the strategy and develop an implementation that is appropriate for you and your technology stack in practice.

The implmentation that will be used today uses Terraform and a GitLab commit tag to deploy a separate instance of the Wordpress application alongside the existing instance.  The Terraform code will create a duplicate set of EC2 instances, an EFS cluster, a Memcached instance, and an RDS database in the same VPC as the production Wordpress deployment.  Once deployed this staged environment will be made available to receive traffic or undergo further testing.  When satisfied you can then promote the staged environment into production by routing production traffic to the staged instance using Route53 or similar means to direct production traffic.

## Learning Objectives:

- See one way to perform a blue / green deployment on the EC2-based infrastructure for Wordpress

## 01 Create a way to track the production deployment

To be able to reference and work with the production instance a reference to the production deployment will be required.  Use AWS Parameter Store to keep a point to the live production deployment of Wordpress.

1. To start the development of a blue / green deployment strategy first create a new branch.

    ```bash
    git checkout -b bg_development
    ```

1. Create a `live.release` variable in variables.tf to reference the current production deployment.

    ```terraform
    variable "live_release" {
        description = "Current live release name"
        type = string
        default = "base"
    }
    ```

1. Now set the current release as the 'live' release:

    ```bash
    aws ssm put-parameter --name 'live.release' --value 'base' --type String --region 'eu-west-1'
    ```

## 02 Modify the Database to Support Updates

When deployed, the staged RDS instance will be a clone of the production database.  To do this the `live.release` reference defined above will be used to retrieve the latest RDS snapshot of the production database.  Later on Ansible will be used to update the database schema of the database.

1. Now update the `database.tf` file, modifying the `aws_db_instance` resource to populate itself based on the latest snapshot of the production database.  Add the following `data` resource to your `database.tf` file to reference the most recent snapshot of the production database.  Then update the `aws_db_instance` resource to reference it:

    ```terraform
    data "aws_db_snapshot" "latest_live_snapshot" {
      db_instance_identifier = "${var.rds_db_identifier}-${var.live_release}"
      most_recent            = true
      count                  = var.live_release != "" ? 1 : 0
    }
    ```

    ```terraform
    resource "aws_db_instance" "wordpress" {
      allocated_storage       = 20
      identifier              = "${var.rds_db_identifier}-${var.release}"
      snapshot_identifier     = var.live_release != "" ? data.aws_db_snapshot.latest_live_snapshot[0].id : null
    ```

## 03 Modify the Ansible playbook to upgrade Wordpress, not just install it

In this instance you are only cloning the production database, but you would also want to clone the production filesystem.  Using AWS DataSync in combination with EFS would achieve this but for brevity this has been left out.  However the Ansible playbook has been updated to update the Wordpress installation on the filesystem, update the database schema, update the plugins, and to update the Wordpress themse as part of its execution.

1. Update your Ansible playbook to perform an update of its database, its themes, and its plugins.  The contents for the updated playbook with the additional commands can be found in the repository, update your `wordpress.yaml` with the contents of [`ansible/wordpress.yaml`](../src/ansible/v2/wordpress.yaml).

## 04 Modify CI configuration to perform a blue / green update

Finally update the CI configuration to use the GitLab Commit reference to give Terraform a statefile independent of the production release.  Also use the AWS CLI to interact with Parameter Store - to read the reference to the production deployment and to create a pointer to the soon-to-be-staged Wordpress deployment.

1. In addition to applying the Terraform GitLab will also need to work with AWS Parameter Store to retrieve the current production release reference, and to publish the staged deployment.  Update your `.gitlab-ci.yml` with the following:

    ```yaml
    # Entrypoint is also needed as image by default set `terraform` binary as an
    # entrypoint.
    image:
      name: registry.gitlab.com/gitlab-org/gitlab-build-images:terraform
      entrypoint:
        - "/usr/bin/env"
        - "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

    # Default output file for Terraform plan
    variables:
      PLAN_FILE: plan.tfplan
      JSON_PLAN_FILE: tfplan.json
      JSON_PLAN_SUMMARY_FILE: tfplan_summary.json
      AWS_REGION: eu-west-1

    cache:
      paths:
        - .terraform

    before_script:
      - alias convert_report="jq -r '([.resource_changes[]?.change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'"
      - env
      - apk add curl aws-cli
      - terraform --version
      - curl -L "$(curl -Ls https://api.github.com/repos/terraform-linters/tflint/releases/latest | grep -o -E "https://.+?_linux_amd64.zip")" -o tflint.zip && unzip tflint.zip && rm tflint.zip && chmod 0755 tflint && mv tflint /usr/local/bin
      - tflint --version

    stages:
      - init
      - validate
      - build
      - deploy
      
    init-job:
      stage: init
      script:
        - terraform init -backend-config="key=project/$CI_COMMIT_REF_SLUG/terraform.tfstate"

    validate-job:
      stage: validate
      script:
        - terraform fmt
        - terraform validate
        - tflint

    plan-job:
      stage: build
      script:
        - PREV_RELEASE=$(aws ssm describe-parameters --parameter-filters 'Key=Name,Values=live.release' --region $AWS_REGION --query 'Parameters[*].Name' --output text)
        - LIVE_RELEASE=$([ ! -z $PREV_RELEASE ] && aws ssm get-parameter --name 'live.release' --query 'Parameter.Value' --output text --region $AWS_REGION || echo '')
        - echo PREV_RELEASE=$PREV_RELEASE
        - echo LIVE_RELEASE=$LIVE_RELEASE
        - terraform plan -out=$PLAN_FILE -var-file=terraform.tfvars -var="release=$CI_COMMIT_REF_SLUG" -var="live_release=$LIVE_RELEASE"
        - echo Producing JSON version of plan file
        - terraform show --json $PLAN_FILE > $JSON_PLAN_FILE
        - terraform show --json $PLAN_FILE | convert_report > $JSON_PLAN_SUMMARY_FILE
      artifacts:
        paths:
          - $PLAN_FILE
          - $JSON_PLAN_FILE
          - $JSON_PLAN_SUMMARY_FILE
        reports:
          terraform: $JSON_PLAN_SUMMARY_FILE

    # Separate apply job for manual launching Terraform as it can be a destructive action
    apply-job:
      stage: deploy
      environment:
        name: production
      script:
        - terraform apply -input=false $PLAN_FILE
        - LB_URL=$(terraform output -json | jq '.website_url.value')
        - aws ssm put-parameter --name 'staged.release' --value "$CI_COMMIT_REF_SLUG" --type String --overwrite --region $AWS_REGION
        - aws ssm put-parameter --name 'staged.url' --value "$LB_URL" --type String --overwrite --region $AWS_REGION
      dependencies:
        - plan-job
      rules:
        - if: $CI_COMMIT_TAG && '$CI_COMMIT_BRANCH == "master"'
          when: manual
    ```

## 05 Merge the changes

The CI configuration is now updated to deploy a second instance of Wordpress.  Commit these changes back to the project for validation, merging, and release.  Please note that while the `apply-job` still requires a manual release it has an additional condition which means only *tagged* releases will be deployed.

1. Commit your changes back to the repository for validation:

    ```bash
    git add *.tf
    git add .gitlab-ci.yml
    git add ansible/wordpress.yaml
    git commit -m 'added ability for blue/green deployment'
    git push --set-upstream origin bg_development
    ```

1. When the changes have validated perform a merge request to merge them with the main branch.

1. When the pipeline has completed the validation of the merge with the main branch you must tag the commit to allow its release into staging.  To tag the release click **_Repository -> Commits_** on the left of the GitLab UI.  

1. Click on the merge commit most recently performed.  

1. Click the **_Options_** button in the upper-right and select **_Tag_**.  

1. Enter a *Tag name* such as 'wordpress-v5.5' and click **_Create tag_**.

1. The pipeline will once again execute, after the build stage be sure and release the pipeline to run the **_Apply_** job.

1. When the deployment has occured visit the [AWS Parameter Store console](https://console.aws.amazon.com/systems-manager/parameters/?tab=Table) to retrieve the URL of the staged environment.  Alternatively you can use the AWS CLI:

    ```bash
    aws ssm get-parameter --name 'staged.url' --region eu-west-1 
    ```

1. Open the URL in your web browser, sign in, and you should now have a completely up-to-date Wordpress installation.

---

## Summary

In this lab you modified your CI configuration to support blue / green deployments of your code base.  With the addition of a step to promote your staged environments into production you would have a complete blue / green deployment implementation.  To find out more about how to implement blue / green deployment strategies on AWS please visit this [article from ThoughtWorks](https://www.thoughtworks.com/insights/blog/implementing-blue-green-deployments-aws).