# Deploy Wordpress

## Overview

You have created some baseline infrastructure and Terraform code in the previous lab. In this lab you will now create an update your Terraform code to also provision PHP application servers to host the Wordpress application. To do this you will create Terraform code that creates an auto-scaling group of virtual servers that are linked with an application load balancer to distribute requests across those servers. You will also create an Ansible playbook which is designed to configure the host operating system and install the PHP and Wordpress application.

Once created the auto-scaling group will create 1 or more new virtual machines as defined by your launch template. When the server starts it will execute the Ansible playbook to configure the server operating system and software. The server will then be joined to the load balancer by the auto-scaling group and the server can begin serving traffic.

## Learning Objectives:

- Add an autoscaling group of webapp servers
- Add Ansible to the user data to install Wordpress

## 01 Configure the Wordpress Infrastructure

1. Return to the main branch of your project repository and update your local version with the merged branch on GitLab.  Then create a new branch for this next set of changes.

    ```bash
    git checkout master
    git pull
    git checkout -b add_compute
    ```

1. To begin, add code to your `wordpress.tf` template to provision the auto-scaling group of servers. Replace the contents of your existing `wordpress.tf` with the contents of [wordpress.tf](../src/terraform/v2/wordpress.tf).

1. To configure the PHP application server and operating system (install Wordpress, mount NFS, initialize the database if needed, etc) you will use a combination of Bash shell scripts and Ansible. Start by creating an Ansible playbook. In your project make a directory named `ansible` containing a file named `wordpress.yaml`. The contents of this file can be found in [wordpress.yaml](../src/ansible/v1/wordpress.yaml).

   > If you inspect the `wordpress.tf` template you will find that Terraform uploads the Ansible playbook to Amazon S3 when it creates the infrastructure. In the next step you will update the startup shell script for your PHP application server to retreive and execute the playbook from Amazon S3.

1. Next configure your PHP application server to run this Ansible playbook when the server is first created by modifying the `user_data` field in your `wordpress.tf` (around line 185) to read as follows:

   ```terraform
   user_data = <<EOF
   #!/bin/bash

   yum update -y
   yum install -y python27-pip
   pip install ansible

   export AWS_REGION=${var.aws_region}
   export EFS_FS_ID=${module.efs.id}
   export DB_NAME=${var.db_name}
   export DB_USERNAME=${var.db_username}
   export DB_PASSWORD=${var.db_password}
   export DB_HOSTNAME=${aws_db_instance.wordpress.address}
   export WP_ADMIN=${var.wp_admin}
   export WP_PASSWORD=${var.wp_password}
   export LB_HOSTNAME=${module.alb.this_lb_dns_name}

   aws s3 cp s3://${aws_s3_bucket.resource_bucket.id}/${aws_s3_bucket_object.wordpress_playbook.id} /tmp/wordpress.yaml
   /usr/local/bin/ansible-playbook /tmp/wordpress.yaml
   EOF
   ```

   > The Ansible playbook configures the mount point for the NFS share and configures the Wordpress application to use the RDS database and ElastiCache instances.  To do this it needs the hostnames and username / passwords that are used to access these systems.  To transfer this information from Terraform to Ansible the above script uses value interpolation in Terraform to inject the values as environment variables on the application server.  The Playbook will then import these environment variables to use them in the Ansible playbook.

1. Lastly, add an additional output to `outputs.tf` so that you can easily find the hostname of your new Wordpress website:

   ```terraform
   output "website_url" {
     value = "http://${module.alb.this_lb_dns_name}"
   }
   ```

1. Commit the added and updated files and push them to GitLab for deployment.

   ```bash
   git add ansible/wordpress.yaml
   git add *.tf
   git commit -m 'completed wordpress configuration'
   git push --set-upstream origin add_compute
   ```

1. As in the last lab GitLab will execute the validation and build of your new branch.  When this has completed, again create a merge request to merge the changes with the main branch and accept the merge request.

1. After the validation and build of the main branch has completed, approve the deployment of the code to AWS as in the previous lab.

1. After approximately 10 minutes the server will have started, installed Wordpress, configured the database, and will be accessible via the load balancer URL.

1. To access your new Wordpress website read the output of the `apply-job` stage of your pipeline. The Terraform output will include the hostname of the loadbalancer, for example:

   ```
   Apply complete! Resources: 41 added, 0 changed, 0 destroyed.
   Outputs:
   website_url = http://wordpress-alb-130952432.eu-west-3.elb.amazonaws.com
   ```

   > To log-in to your Wordpress website use the administrator username and password defined in your `variables.tf` file.

## Summary

You have now created infrastructure as code to configure and deploy a highly available Wordpress content management system on AWS using HashiCorp Terraform, GitLab, Ansible, and a GitLab Runner. Terraform, through the use of its providers can configure a wide range of service providers by defining additional resources in your Terraform templates. GitLab can then manage those versions and apply the Terraform templates using GitLab Runners as the code is developed. Complex workflows can be orchestrated through Git branches to orchestrate code delivery into different accounts, different regions, and can also be used to automate version releases through blue / green or canary deployments.

### Extra Credit

If there is still time in the lab, and if you are so inclined, please try an additional lab to see how you can build more complex workflows using GitLab CI.

 - [Perform Blue / Green Deployment with GitLab and Terraform](lab_04b_blue_green_deployment.md)

 ---

Please visit the following links to learn more about the topics touched on in this workshop:

- [Practicing Continuous Integration and Continous Delivery on AWS Whitepaper](https://d1.awsstatic.com/whitepapers/DevOps/practicing-continuous-integration-continuous-delivery-on-AWS.pdf)
- [AWS Re:Invent - Advanced Continuous Delivery Best Practices (video)](https://youtu.be/Jnl29J3RJQ4)
- [Incremental Rollouts with GitLab CI/CD](https://docs.gitlab.com/ee/ci/environments/incremental_rollouts.html)
- [Terraform Recommended Practices](https://www.terraform.io/docs/cloud/guides/recommended-practices/index.html)
- [Terraform Best Practices](https://www.terraform-best-practices.com/)
- [How Infrastructure Teams use GitLab and Terraform for GitOps](https://about.gitlab.com/blog/2019/11/12/gitops-part-2/)
