# Getting Started with GitLab

## Overview

In addition to a workspace you will also need a version control system and build system. To do that you will create a free account with GitLab.com to create a Git repository for your Terraform project. You will then deploy a GitLab Runner into AWS to provide a build system for your project. Finally you will create a basic GitLab CI configuration to be executed by the GitLab Runner.

The GitLab Runner will leverage AWS Spot Instances to provide a low cost virtual machine to execute builds. The virtual machines will be provisioned to execute a build and will then be destroyed when they are no longer needed in order to further reduce costs, [inline with recommended practice](https://about.gitlab.com/blog/2017/11/23/autoscale-ci-runners/). The GitLab Runner will provision just-in-time virtual machines to execute your build, destroying the build servers when they are no longer needed.

![Auto-scaling GitLab Runner](imgs/gitlab-runner.png)

## Learning Objectives:

- Create a GitLab account and project repository
- Deploy a GitLab CI Runner for the repository
- Clone the repository to Cloud9 and create a basic gitlab-ci.yml

## 01 Create a GitLab Project

1.  Visit https://gitlab.com

1.  If you have an account already skip to step 5

1.  Register for an account (click "Register" on the top right of the landing page), they're free! 
    ![GitLab Register Banner](imgs/gitlab-register-banner.png "GitLab Register Banner")

1.  Fill in the appropriate information and click "Register"  
**Note:** Please keep your username and password handy, as you'll need them to clone/push changes later on...
    ![GitLab Register Form](imgs/gitlab-register.png "GitLab Register Form")

1.  When on the GitLab dashboard, click "New Project" 
    ![GitLab New Project](imgs/gitlab-new-project.png "GitLab New Project")

1.  Select "Create Blank Project"
    ![Create Blank Project](imgs/gitlab-blank-proj.png "Create Blank Project")

1.  Fill in the information, as shown below and select "Create Project" 
    ![GitLab New Project Form](imgs/gitlab-new-proj.png "GitLab New Project Form")

## 02 Deploy a GitLab Runner

1.  Visit **_Settings -> CI / CD_** for your GitLab project  
    ![GitLab Settings CICD](imgs/gitlab-cicd.png "GitLab Settings CI/CD")

1.  Expand **_Runners_** and copy the registration token provided under **_Specific Runners_**.  
    For example: `HG1zekDbfJuEwZCtjDMN`  
    ![Specific Runner Token](imgs/gitlab-specific-runner.png "Specific Runner Token")

1.  Return to your Cloud9 IDE and update `terraform.tfvars` of the gitlab-runner-tf repository with the registration token from GitLab

1.  Also ensure that the `aws_region` in your `terraform.tfvars` is set to an appropriate value such as `eu-west-1`.

1.  Using the command line terminal in your IDE, change directory to `gitlab-runner-tf` and deploy your Terraform:

    ```bash
    cd gitlab-runner-tf
    terraform init
    terraform apply -auto-approve -var-file terraform.tfvars
    ```

    > When Terraform completes make a note of the outputs it provides, specifically the AWS Region in which it deployed the runner, the name of the Amazon S3 bucket it created to hold Terraform State files, and the DynamoDB table that will be used to establish locks for the ability to write to the State files in S3.

1.  When the Terraform script has completed (average time to complete is 5 minutes), confirm the runner has registered with GitLab by visiting your project's **_Settings -> CI / CD_**, and expand **_Runners_**.  It can take up to 5 minutes after the Terraform script has completed to register with GitLab.

1.  You should see your runner registered under **_Specific Runners_**

1.  Lastly click **_Disable shared runners_** so that all CI work is handed to your runner

## 03 Configure your Project's CI Pipeline

1.  Copy your GitLab project's HTTPS clone url
    ![GitLab Clone](imgs/gitlab-clone.png "GitLab Clone")

1.  Switch back to your Cloud9 IDE

1. Ensure you are back in the IDE root directory

    ```bash
    cd ~/environment
    ```

1.  Configure your local git environment to store & use your GitLab credentials.  Please note that this is may result in your password being stored in plaintext on your Cloud9 IDE.  This is not advisable for use in a production devlopment environment. (Use a Personal Access Token / SSH)

    ```bash
    git config --global credential.helper store
    git config --global user.name "Your Name"
    git config --global user.email your@email.com
    ```

1.  Clone your GitLab project to your IDE

    ```bash
    git clone https://gitlab.com/xxxx/wordpress-workshop.git
    ```

1. When prompted, enter your username and password that you used to register with GitLab

1. Move into the newly cloned project

    ```bash
    cd wordpress-workshop/
    ```

1. Using the IDE, create a file named `.gitlab-ci.yml` in the root of your wordpress project with the following contents:

    ```yaml
    # Entrypoint is also needed as image by default set `terraform` binary as an
    # entrypoint.
    image:
      name: registry.gitlab.com/gitlab-org/gitlab-build-images:terraform
      entrypoint:
        - "/usr/bin/env"
        - "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

    # Default output file for Terraform plan
    variables:
      PLAN_FILE: plan.tfplan
      JSON_PLAN_FILE: tfplan.json
      JSON_PLAN_SUMMARY_FILE: tfplan_summary.json

    cache:
      paths:
        - .terraform

    before_script:
      - alias convert_report="jq -r '([.resource_changes[]?.change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'"
      - apk add curl
      - terraform --version
      - curl -L "$(curl -Ls https://api.github.com/repos/terraform-linters/tflint/releases/latest | grep -o -E "https://.+?_linux_amd64.zip")" -o tflint.zip && unzip tflint.zip && rm tflint.zip && chmod 0755 tflint && mv tflint /usr/local/bin
      - tflint --version

    stages:
      - init
      - validate

    init-job:
      stage: init
      script:
        - terraform init

    validate-job:
      stage: validate
      script:
        - terraform fmt
        - terraform validate
        - tflint
    ```

1.  Add the new file and commit it to your local repository, then push the change back to GitLab

    ```bash
    git add .gitlab-ci.yml
    git commit -m 'added initial gitlab ci configuration'
    git push
    ```

1.  From the GitLab project page visit **_CI/CD -> Pipelines_** and observe the progress of the execution of your GitLab CI script.
    ![GitLab Pipeline Job Summary](imgs/pipeline-job.png "GitLab Pipeline Job Summary")

## Summary

In this lab you created a GitLab Git repository and deployed a GitLab Runner to build the source code in the repository. You then added a basic GitLab CI configuration designed to lint and validate Terraform code.

In [Lab 02](lab_02_create_base_project.md) you will populate your new project using Terraform and deploy the code onto AWS using your GitLab Runner.
