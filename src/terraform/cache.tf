module "memcached" {
  source = "git::https://github.com/cloudposse/terraform-aws-elasticache-memcached.git?ref=tags/0.8.0"

  name                    = "wordpress-cache-${var.release}"
  vpc_id                  = local.vpc_id
  availability_zones      = ["${var.aws_region}a", "${var.aws_region}b", "${var.aws_region}c"]
  allowed_security_groups = [module.wp_sg.this_security_group_id]
  subnets                 = local.private_subnets
  cluster_size            = 1
  instance_type           = "cache.t3.medium"
  engine_version          = "1.5.16"
  apply_immediately       = true
}
