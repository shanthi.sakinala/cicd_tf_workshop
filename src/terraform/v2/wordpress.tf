data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name = "name"

    values = [
      "amzn-ami-hvm-*-x86_64-gp2",
    ]
  }

  filter {
    name = "owner-alias"

    values = [
      "amazon",
    ]
  }
}

resource "random_id" "stack_id" {
  byte_length = 8
}

######
# ELB
######
module "alb_sg" {
  source = "terraform-aws-modules/security-group/aws//modules/http-80"

  name        = "wp-alb-sg-${random_id.stack_id.hex}"
  description = "Security group for Wordpress ALB with HTTP ports open to WWW"
  vpc_id      = local.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
}

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 5.0"

  name = "wordpress-alb-${var.release}"

  load_balancer_type = "application"

  vpc_id          = local.vpc_id
  subnets         = local.public_subnets
  security_groups = [module.alb_sg.this_security_group_id]

  target_groups = [
    {
      name_prefix      = "wp-tg-"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "instance"
    }
  ]

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
    }
  ]
}

######
# Launch configuration and autoscaling group
######

resource "aws_s3_bucket" "resource_bucket" {
  bucket = "tf-wordpress-rsrc-${random_id.stack_id.hex}"
}

resource "aws_s3_bucket_object" "wordpress_playbook" {
  bucket = aws_s3_bucket.resource_bucket.id
  key    = "ansible/wordpress.yaml"
  source = "ansible/wordpress.yaml"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
  etag = filemd5("ansible/wordpress.yaml")
}

resource "aws_iam_instance_profile" "wordpress_profile" {
  name = "wordpress-server-profile-${random_id.stack_id.hex}"
  role = aws_iam_role.wordpress_role.name
}

resource "aws_iam_role" "wordpress_role" {
  name = "wordpress-server-role-${random_id.stack_id.hex}"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy" "wordpress_policy" {
  name        = "wordpress-server-policy-${random_id.stack_id.hex}"
  description = "IAM Policy for Wordpress application servers"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.resource_bucket.id}/*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "wordpress_permission_attach" {
  role       = aws_iam_role.wordpress_role.name
  policy_arn = aws_iam_policy.wordpress_policy.arn
}

resource "aws_iam_role_policy_attachment" "ssm_permission_attach" {
  role       = aws_iam_role.wordpress_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

module "wp_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "wordpress-sg-${random_id.stack_id.hex}"
  description = "Security group for Wordpress servers"
  vpc_id      = local.vpc_id

  ingress_with_source_security_group_id = [
    {
      rule                     = "http-80-tcp"
      source_security_group_id = module.alb_sg.this_security_group_id
    }
  ]

  egress_rules = ["all-all"]
}

module "wordpress_asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 3.0"

  name = "wordpress-srv-grp"

  # Launch configuration
  #
  # launch_configuration = "my-existing-launch-configuration" # Use the existing launch configuration
  # create_lc = false # disables creation of launch configuration
  lc_name              = "wordpress-lc"
  iam_instance_profile = aws_iam_instance_profile.wordpress_profile.id

  image_id        = data.aws_ami.amazon_linux.id
  instance_type   = "t3.small"
  security_groups = [module.wp_sg.this_security_group_id]

  root_block_device = [
    {
      volume_size = "20"
      volume_type = "gp2"
    }
  ]

  user_data = <<EOF
  #!/bin/bash

  set -e

  yum update -y
  EOF


  # Auto scaling group
  asg_name                  = "wordpress-asg"
  vpc_zone_identifier       = local.private_subnets
  health_check_type         = "EC2"
  min_size                  = 0
  max_size                  = 3
  desired_capacity          = 2
  wait_for_capacity_timeout = 0

  target_group_arns = module.alb.target_group_arns
}
