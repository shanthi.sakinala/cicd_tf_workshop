module "rds_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "wp-rds-sg-${var.release}"
  description = "Security group for Wordpress database"
  vpc_id      = local.vpc_id

  ingress_with_source_security_group_id = [
    {
      rule                     = "mysql-tcp"
      source_security_group_id = module.wp_sg.this_security_group_id
    }
  ]

  egress_rules = ["all-all"]
}

resource "aws_db_subnet_group" "wordpress" {
  subnet_ids = local.private_subnets
}

data "aws_db_snapshot" "latest_live_snapshot" {
  db_instance_identifier = "${var.rds_db_identifier}-${var.live_release}"
  most_recent            = true
  count                  = var.live_release != "" ? 1 : 0
}

resource "aws_db_instance" "wordpress" {
  allocated_storage       = 20
  identifier              = "${var.rds_db_identifier}-${var.release}"
  snapshot_identifier     = var.live_release != "" ? data.aws_db_snapshot.latest_live_snapshot[0].id : null
  storage_type            = "gp2"
  engine                  = "mysql"
  engine_version          = "5.7"
  multi_az                = true
  backup_retention_period = 7
  instance_class          = var.rds_instance_type
  name                    = var.db_name
  username                = var.db_username
  password                = var.db_password
  db_subnet_group_name    = aws_db_subnet_group.wordpress.id
  vpc_security_group_ids = [
    module.rds_sg.this_security_group_id
  ]
  skip_final_snapshot = true
}
