- name: Configure Wordpress server
  hosts: localhost
  become: yes
  gather_facts: yes

  vars:
    aws_region: "{{ lookup('env', 'AWS_REGION') }}"
    efs_file_system_id: "{{ lookup('env', 'EFS_FS_ID') }}"

    db_name: "{{ lookup('env', 'DB_NAME') }}"
    db_hostname: "{{ lookup('env', 'DB_HOSTNAME') }}"
    db_username: "{{ lookup('env', 'DB_USERNAME') }}"
    db_password: "{{ lookup('env', 'DB_PASSWORD') }}"

    wp_admin: "{{ lookup('env', 'WP_ADMIN') | default('wpadmin', True) }}"
    wp_password: "{{ lookup('env', 'WP_PASSWORD') | default('WpPassword$', True) }}"

    lb_hostname: "{{ lookup('env', 'LB_HOSTNAME') }}"

  tasks:
    - name: Install Dependencies
      package:
        name:
          - amazon-efs-utils
          - httpd24
          - mysql56
          - php56
          - php56-devel
          - php56-mysqlnd
          - php56-opcache
          - php56-pecl-igbinary
        state: installed

    - name: Create Wordpress FS mount point
      file:
        path: "/var/www/wordpress"
        state: directory
        mode: 0755

    - name: Get current AZ from AWS.
      uri:
        url: http://169.254.169.254/latest/meta-data/placement/availability-zone
        return_content: yes
      register: aws_current_az

    - name: Ensure EFS volume is mounted.
      mount:
        name: "/var/www/wordpress"
        src: "{{ efs_file_system_id }}.efs.{{ aws_region }}.amazonaws.com:/"
        fstype: nfs4
        opts: nfsvers=4.1
        state: mounted

    - name: Configure httpd
      copy:
        dest: "/etc/httpd/conf.d/wordpress.conf"
        content: |
          ServerName 127.0.0.1:80
          DocumentRoot /var/www/wordpress/wordpress
          <Directory /var/www/wordpress/wordpress>
            Options Indexes FollowSymLinks
            AllowOverride All
            Require all granted
          </Directory>

    - name: Download ElastiCache Client
      unarchive:
        src: https://elasticache-downloads.s3.amazonaws.com/ClusterClient/PHP-5.6/latest-64bit
        dest: /tmp/
        remote_src: yes

    - name: Install ElastiCache Client
      copy:
        src: /tmp/AmazonElastiCacheClusterClient-1.0.0/amazon-elasticache-cluster-client.so
        dest: /usr/lib64/php/5.6/modules/amazon-elasticache-cluster-client.so
        mode: "0644"

    - name: Configure ElastiCache Client
      copy:
        dest: /etc/php-5.6.d/50-memcached.ini
        content: |
          extension=/usr/lib64/php/5.6/modules/amazon-elasticache-cluster-client.so;

    - name: Install IGBinary
      command:
        cmd: /usr/bin/pecl install igbinary-2.0.8

    - name: Download Wordpress Installer
      get_url:
        url: https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
        dest: /bin/wp
        mode: "0755"

    - name: Create Wordpress Install directory
      file:
        path: "/var/www/wordpress/wordpress"
        state: directory
        mode: 0755

    - name: "Download Wordpress"
      command:
        cmd: /bin/wp core download --version='5.5.3' --locale='en_GB' --allow-root
        chdir: /var/www/wordpress/wordpress
        creates: /var/www/wordpress/wordpress/initialized

    - name: "Configure Wordpress"
      command:
        cmd: /bin/wp core config --dbname="{{ db_name }}" --dbuser="{{ db_username }}" --dbpass="{{ db_password }}" --dbhost="{{ db_hostname }}" --dbprefix=wp_ --allow-root
        chdir: /var/www/wordpress/wordpress
        creates: /var/www/wordpress/wordpress/initialized

    - name: "Install Wordpress"
      command:
        cmd: /bin/wp core install --url="http://{{ lb_hostname }}" --title='Wordpress on AWS' --admin_user="{{ wp_admin }}" --admin_password="{{ wp_password }}" --admin_email='admin@example.com' --allow-root
        chdir: /var/www/wordpress/wordpress
        creates: /var/www/wordpress/wordpress/initialized

    - name: "Install Wordpress plugins"
      command:
        cmd: /bin/wp plugin install w3-total-cache --allow-root
        chdir: /var/www/wordpress/wordpress
        creates: /var/www/wordpress/wordpress/initialized

    - name: "Upgrade Wordpress"
      command:
        cmd: /bin/wp core update --version='5.5.3' --force --allow-root
        chdir: /var/www/wordpress/wordpress
        creates: /var/www/wordpress/wordpress/initialized

    - name: "Upgrade Wordpress DB"
      command:
        cmd: /bin/wp core update-db --allow-root
        chdir: /var/www/wordpress/wordpress
        creates: /var/www/wordpress/wordpress/initialized

    - name: "Upgrade Wordpress Plugins"
      command:
        cmd: /bin/wp plugin update --all --allow-root
        chdir: /var/www/wordpress/wordpress
        creates: /var/www/wordpress/wordpress/initialized

    - name: "Upgrade Wordpress Themes"
      command:
        cmd: /bin/wp theme update --all --allow-root && /bin/wp theme activate twentyseventeen --allow-root
        chdir: /var/www/wordpress/wordpress
        creates: /var/www/wordpress/wordpress/initialized

    - name: "Update Wordpress siteurl"
      command:
        cmd: /bin/wp option set siteurl "http://{{ lb_hostname }}" --allow-root
        chdir: /var/www/wordpress/wordpress
        creates: /var/www/wordpress/wordpress/initialized

    - name: Record Wordpress initialization
      file:
        path: /var/www/wordpress/wordpress/initialized
        state: touch
        mode: 0444

    - name: Create Opcache Directory
      file:
        path: /var/www/.opcache
        state: directory
        mode: 0755

    - name: Configure opcache file cache
      replace:
        path: /etc/php-5.6.d/10-opcache.ini
        regexp: ";opcache.file_cache=.*"
        replace: 'opcache.file_cache=\/var\/www\/.opcache/'

    - name: Configure opcache memory
      replace:
        path: /etc/php-5.6.d/10-opcache.ini
        regexp: "opcache.memory_consumption=.*"
        replace: "opcache.memory_consumption=512"

    - name: Download OPCache PHP
      get_url:
        url: https://s3.amazonaws.com/aws-refarch/wordpress/latest/bits/opcache-instanceid.php
        dest: /var/www/wordpress/wordpress/opcache-instanceid.php
        mode: "0755"

    - name: Give apache ownership of Wordpress
      file:
        path: /var/www/wordpress/wordpress
        state: directory
        recurse: yes
        owner: apache
        group: apache

    - name: Make Wordpress executable
      file:
        path: /var/www/wordpress/wordpress/wp-content
        recurse: yes
        mode: 0755

    - name: Restart httpd
      service:
        name: httpd
        state: restarted
        enabled: yes